package com.example.projetandroid;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private ArrayList<TodoItem> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this casemTextView;
        public TextView myTextView;
        public ImageView prio;

        public MyViewHolder(View v) {
            super(v);
            myTextView = itemView.findViewById(R.id.todo_label);
            prio = itemView.findViewById(R.id.priority_tag);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerAdapter(ArrayList<TodoItem> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.myTextView.setText(mDataset.get(position).getLabel());
        switch (mDataset.get(position).getTag()){
            case Faible:
                holder.prio.setBackgroundColor(Color.rgb(46, 204, 113));
                break;
            case Normal:
                holder.prio.setBackgroundColor(Color.rgb(241, 196, 15));
                break;
            case Important:
                holder.prio.setBackgroundColor(Color.rgb(192, 57, 43));
                break;
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}